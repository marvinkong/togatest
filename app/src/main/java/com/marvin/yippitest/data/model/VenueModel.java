package com.marvin.yippitest.data.model;

import java.io.Serializable;

public class VenueModel implements Serializable {

    public int id;
    public String imageUrl;
    public boolean isClose;
    public String closeLabel;
    public String productName;
    public String productDesc;
    public double star;
    public String distance;
    public String promoDesc;
    public int outletAround;
}
