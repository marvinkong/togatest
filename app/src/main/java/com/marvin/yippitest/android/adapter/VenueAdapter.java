package com.marvin.yippitest.android.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.marvin.yippitest.R;
import com.marvin.yippitest.data.model.VenueModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.ViewHolder> {

    private Context context;
    private ArrayList<VenueModel> data = new ArrayList<>();
    private ItemClickInterface itemClickInterface;

    public interface ItemClickInterface {
        void onItemClicked(VenueModel venueModel);
    }

    public VenueAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VenueAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_venue_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final VenueModel venueModel = data.get(position);

        if (venueModel != null) {

            setTextIfNotNull(holder.venueTV, venueModel.productName);
            setTextIfNotNull(holder.descriptionTV, venueModel.productDesc);
            setTextIfNotNull(holder.ratingTV, String.format(Locale.getDefault(), "%.1f", venueModel.star));
            setTextIfNotNull(holder.distanceTV, venueModel.distance);
            setTextIfNotNull(holder.discountTV, venueModel.promoDesc);

            holder.nearbyTV.setText(context.getString(R.string.nearby_outlets, venueModel.outletAround));
            holder.nearbyTV.setVisibility(venueModel.outletAround > 0 ? View.VISIBLE : View.GONE);
            holder.overlayTV.setText(venueModel.closeLabel);
            holder.overlayTV.setVisibility(venueModel.isClose ? View.VISIBLE : View.GONE);
            holder.overlayIV.setVisibility(venueModel.isClose ? View.VISIBLE : View.GONE);

            Glide.with(context)
                    .load(venueModel.imageUrl)
                    .placeholder(android.R.color.darker_gray)
                    .centerCrop()
                    .into(holder.venueIV);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickInterface != null) {
                        itemClickInterface.onItemClicked(venueModel);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private void setTextIfNotNull(TextView textView, String text) {
        if (!TextUtils.isEmpty(text)) {
            textView.setText(text);
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    public void setData(List<VenueModel> list) {
        data.clear();
        data.addAll(list);
        notifyDataSetChanged();
    }

    public void setItemClickInterface(ItemClickInterface itemClickInterface) {
        this.itemClickInterface = itemClickInterface;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView venueIV;
        ImageView overlayIV;
        TextView overlayTV;
        TextView venueTV;
        TextView descriptionTV;
        TextView ratingTV;
        TextView distanceTV;
        TextView discountTV;
        TextView nearbyTV;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            venueIV = itemView.findViewById(R.id.venueIV);
            overlayIV = itemView.findViewById(R.id.overlayIV);
            overlayTV = itemView.findViewById(R.id.overlayTV);
            venueTV = itemView.findViewById(R.id.venueTV);
            descriptionTV = itemView.findViewById(R.id.descriptionTV);
            ratingTV = itemView.findViewById(R.id.ratingTV);
            distanceTV = itemView.findViewById(R.id.distanceTV);
            discountTV = itemView.findViewById(R.id.discountTV);
            nearbyTV = itemView.findViewById(R.id.nearbyTV);
        }
    }
}
