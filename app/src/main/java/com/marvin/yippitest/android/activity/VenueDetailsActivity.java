package com.marvin.yippitest.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.marvin.yippitest.R;
import com.marvin.yippitest.data.model.VenueModel;

import java.util.Locale;

public class VenueDetailsActivity extends AppCompatActivity {

    private ImageView venueIV;
    private View scrimV;
    private TextView descriptionTV;
    private TextView ratingTV;
    private TextView distanceTV;
    private TextView discountTV;
    private TextView nearbyTV;
    private CollapsingToolbarLayout toolBarLayout;

    private CollapsingToolbarLayoutState state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_details);

        setupViews();

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            VenueModel venueModel = (VenueModel) intent.getExtras().getSerializable("venue");
            displayData(venueModel);
        }
    }

    private void setupViews() {
        venueIV = findViewById(R.id.venueIV);
        scrimV = findViewById(R.id.scrimV);
        descriptionTV = findViewById(R.id.descriptionTV);
        ratingTV = findViewById(R.id.ratingTV);
        distanceTV = findViewById(R.id.distanceTV);
        discountTV = findViewById(R.id.discountTV);
        nearbyTV = findViewById(R.id.nearbyTV);

        Toolbar toolbar = findViewById(R.id.defaultTB);
        setSupportActionBar(toolbar);
        toolBarLayout = findViewById(R.id.defaultCTL);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        AppBarLayout appBarLayout = findViewById(R.id.defaultABL);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (verticalOffset == 0) {
                    if (state != CollapsingToolbarLayoutState.EXPANDED) {
                        state = CollapsingToolbarLayoutState.EXPANDED;
                        scrimV.setVisibility(View.GONE);
                    }
                } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    if (state != CollapsingToolbarLayoutState.COLLAPSED) {
                        state = CollapsingToolbarLayoutState.COLLAPSED;
                        scrimV.setVisibility(View.GONE);
                    }
                } else {
                    if (state != CollapsingToolbarLayoutState.INTERNEDIATE) {
                        state = CollapsingToolbarLayoutState.INTERNEDIATE;
                    }
                }
            }
        });
    }

    private void displayData(VenueModel venueModel) {


        if (venueModel != null) {
            if (toolBarLayout != null) {
                toolBarLayout.setTitle(venueModel.productName);
            }

            descriptionTV.setText(venueModel.productDesc);
            ratingTV.setText(String.format(Locale.getDefault(), "%.1f", venueModel.star));
            distanceTV.setText(venueModel.distance);
            discountTV.setText(venueModel.promoDesc);
            nearbyTV.setText(getString(R.string.nearby_outlets, venueModel.outletAround));
            nearbyTV.setVisibility(venueModel.outletAround > 0 ? View.VISIBLE : View.GONE);

            Glide.with(this)
                    .load(venueModel.imageUrl)
                    .placeholder(android.R.color.darker_gray)
                    .into(venueIV);
        }
    }


    private enum CollapsingToolbarLayoutState {
        EXPANDED,
        COLLAPSED,
        INTERNEDIATE
    }
}