package com.marvin.yippitest.android.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.marvin.yippitest.R;
import com.marvin.yippitest.android.Utils;
import com.marvin.yippitest.android.adapter.VenueAdapter;
import com.marvin.yippitest.data.model.VenueModel;
import com.marvin.yippitest.server.ApiInterface;
import com.marvin.yippitest.server.RetrofitClient;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private RecyclerView defaultRV;
    private TextView placeholderTV;
    private ProgressDialog progressDialog;

    private VenueAdapter venueAdapter;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        defaultRV = findViewById(R.id.defaultRV);
        placeholderTV = findViewById(R.id.placeholderTV);

        if (!Utils.isNetworkConnected(this)) {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        setupList();
    }

    private void setupList() {
        venueAdapter = new VenueAdapter(this);
        venueAdapter.setItemClickInterface(new VenueAdapter.ItemClickInterface() {
            @Override
            public void onItemClicked(VenueModel venueModel) {
                startVenueDetails(venueModel);
            }
        });

        defaultRV.setAdapter(venueAdapter);
        defaultRV.setLayoutManager(new LinearLayoutManager(this));

        Retrofit retrofit = RetrofitClient.getInstance();
        apiInterface = retrofit.create(ApiInterface.class);

        fetchData();
    }

    private void fetchData() {
        apiInterface.getVenues()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<VenueModel>>() {
                    @Override
                    public void onSuccess(List<VenueModel> venueModels) {
                        displayData(venueModels);
                        showProgressDialog(false);
                    }

                    @Override
                    protected void onStart() {
                        super.onStart();
                        showProgressDialog(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showProgressDialog(false);
                    }
                });
    }

    private void displayData(List<VenueModel> venueModelList) {
        if (venueModelList != null && venueModelList.size() > 0) {
            venueAdapter.setData(venueModelList);

            defaultRV.setVisibility(View.VISIBLE);
            placeholderTV.setVisibility(View.GONE);
        } else {
            defaultRV.setVisibility(View.GONE);
            placeholderTV.setVisibility(View.VISIBLE);
        }
    }

    private void startVenueDetails(VenueModel venueModel) {
        Intent yourIntent = new Intent(this, VenueDetailsActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("venue", venueModel);
        yourIntent.putExtras(b);

        startActivity(yourIntent);
    }

    private void showProgressDialog(boolean flag) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        }

        if (flag) {
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }
}