package com.marvin.yippitest.server;

import com.marvin.yippitest.data.model.VenueModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("/get/")
    Single<List<VenueModel>> getVenues();
}
